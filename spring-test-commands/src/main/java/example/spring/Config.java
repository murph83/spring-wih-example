package example.spring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Config {
	
	@Bean
	public MessageBean messageBean(){
		return new MessageBean();
	}
	
	
	public static ApplicationContext beanLoader(ClassLoader classLoader){
		AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
		ctx.setClassLoader(classLoader);
		
		ctx.register(Config.class);
		ctx.refresh();
		
		return ctx;
		
	}
	
	public static class MessageBean {
		public String message = "Hello";
	}

}
