package example.workitem;

import java.util.Collections;

import org.jbpm.process.workitem.AbstractLogOrThrowWorkItemHandler;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import example.spring.Config;
import example.spring.Config.MessageBean;

public class SpringLoadingWorkitemHandler extends AbstractLogOrThrowWorkItemHandler {
	
	final ApplicationContext ctx;
	final Logger LOG = LoggerFactory.getLogger(SpringLoadingWorkitemHandler.class);
	
	public SpringLoadingWorkitemHandler(ClassLoader classLoader) {
		LOG.info("Loading Spring Context with Classloader: {}", classLoader);
		ctx = Config.beanLoader(classLoader);
	}

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		MessageBean mb = ctx.getBean(MessageBean.class);
		LOG.info(mb.message);
		manager.completeWorkItem(workItem.getId(), Collections.<String, Object>emptyMap());
		
	}

	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		// TODO Auto-generated method stub
		
	}
	
	

}
